package main

type ENC struct {
	Environment string   `yaml:environment`
	Classes     []string `yaml:classes`
}

func (e *ENC) setEnvironment(environment string) {
	e.Environment = environment
}

func (e *ENC) addClass(class string) {
	e.Classes = append(e.Classes, class)
}

func (e *ENC) getEnvironment() string {
	return e.Environment
}

func (e *ENC) getClasses() []string {
	return e.Classes
}
