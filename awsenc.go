package main

import (
	"fmt"
	"github.com/awslabs/aws-sdk-go/aws"
	"github.com/awslabs/aws-sdk-go/service/ec2"
	"gopkg.in/yaml.v2"
	"log"
	"os"
	"strings"
)

func main() {

	enc := new(ENC)

	if len(os.Args) < 2 {
		fmt.Printf("Usage: %v <hostname>\n", os.Args[0])
		os.Exit(1)
	}

	host := strings.Split(os.Args[1], ".")
	svc := ec2.New(&aws.Config{Region: "us-west-2"})

	enc.setEnvironment(host[1])

	params := ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("tag:Name"),
				Values: []*string{
					aws.String(host[0]),
				},
			},
			&ec2.Filter{
				Name: aws.String("tag:Environment"),
				Values: []*string{
					aws.String(host[1]),
				},
			},
		},
	}

	request := ec2.DescribeInstancesInput(params)

	resp, err := svc.DescribeInstances(&request)
	if err != nil {
		panic(err)
	}

	for idx, _ := range resp.Reservations {
		for _, inst := range resp.Reservations[idx].Instances {
			for _, tag := range inst.Tags {
				if *tag.Key == "Classes" {
					for _, tag := range strings.Split(*tag.Value, ",") {
						enc.addClass(tag)
					}
				}
			}
		}
	}

	y, err := yaml.Marshal(enc)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	if len(enc.getClasses()) != 0 {
		fmt.Printf("---\n%v\n", string(y))
	}

}
