# awsenc

This is a Puppet External Node Classifier which relies on AWS tags to assign classes to your instance.


# setup

Your nodes should have the following tags:

* `Classes: comma,separated,list,of,puppet,classes`
* `Name: non-fqdn hostname`

Your node should be named:
<hostname>.<environment>.<domain>

# usage

`./awsenc <hostname>`


# todo

1. Make it less opinionated
  * I'd like to figure out a way to have a different hostname scheme

